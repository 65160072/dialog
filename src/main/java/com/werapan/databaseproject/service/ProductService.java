/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class ProductService {
    private final ProductDao productdao = new ProductDao();
    public ArrayList<Product> getProductsOderByname(){
        return (ArrayList<Product>) productdao.getAll(" product_name ASC ");
    }
}
